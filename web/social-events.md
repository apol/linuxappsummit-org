---
layout: page
title: "Social Events"
permalink: /social-events/
---
# Join the fun!

One of the most important, and enjoyable, parts of going to a conference is 
getting to know the other partcipants. Whether it's to network professionaly, 
make new friends, or, ideally, both, social events play an important part in 
making that happen! We invite you to join us for the following events.


### Tuesday (12 Nov) and Wednesday (13 Nov)- Walking Tour
Those interested can join us for a walking tour of Barcelona! You should have 
enough time after the conference ends that day to drop off your bags before the 
tour begins. Remember to bring warm clothing, good walking shoes, and an 
umbrella, just in case it rains.

 * **Meeting Location and Time:** meet in front of Casa Batlló at 19:00. Tour duration is 1.5 hrs.
 * **Cost:** 5 EUR suggested donation each, plus any drinks or food at the bar where we end up.

**Tour Description:** We will visit the city center and Gothic quarter. The tour 
will start at the metro station Passeig de Gràcia, in front of Casa Batlló. 
From Passeig de Gracia (the boulevard with the modern-style houses by the 
architect Gaudí), we will walk down to the Gothic Quarter (the heart of 
Barcelona) to see Els Quatre Gats (the Café of the artists), the cathedral, 
the remains of the roman temple, the medieval king's palace, Sant Felip Neri 
square, the Jewish quarter, Santa Maria del Pi Gothic church, among other 
monuments and historic corners, reaching the popular promenade La Rambla and 
arriving at Sant Antoni area, where we will finish the walk to enjoy a beer at 
a local brewery (Fàbrica Moritz).


Due to lots of interest, there will be two groups. You can find more information 
on groups here: [Walking Tour Groups](https://las2019.bcnfs.org/doku.php?id=wiki:bcntour).

### Wednesday, 13th of November - Sponsors Dinner (by invitation only)
We will be hosting a special dinner for our sponsors. Details have been sent to 
participants, but if you have any questions, or would be interested in joining 
a future dinner, please let one of the organizers know. 
We are always interested in new sponsors :)

If you are not participating in the sponsors dinner, we encourage you to explore 
the city! Check out our list of 
[tips and tricks](https://las2019.bcnfs.org/doku.php?id=wiki:tips), 
which includes restaurant suggestions from the locals.

### Thursday, 14th of November - Social Event
Join us for a complimentary drink and some delicious tapas as we close the 
core days of the conference. 

Feel free to bring friends! Just remember, drinks and tapas are first come, 
first served, so make sure you arrive early in the evening.

 * **Time:** 19:00 onward
 * **Location:** ArteSants bar (Carrer Guadiana, 8, 08014 Barcelona, Spain)