---
layout: page
title: "Accommodations"
permalink: /accommodations/
---

# Accommodations

There are many nearby accommodations to the venue because of the convenient location. Below, you can find a few suggestions.

### Smart Room Barcelona
<a href="http://www.smartroombarcelona.com">Website</a>  
~56€/night (Breakfast included)  
2 min walk to venue  

### Live and Dream Hostel Barcelona
<a href="https://live-dream.hotelbcn-barcelona.com/">Website</a>  
~75€/night (Breakfast not included)  
3 min walk to venue  

### Hostal Sans Barcelona
<a href="http://www.hostalsans.com/">Website</a>  
~55€/night (Breakfast not included) or ~65€/night (Breakfast included)  
5 min walk to venue
