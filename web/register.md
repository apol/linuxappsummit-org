---
layout: page
title: "Register"
permalink: /register
---
# Registration is now open!
The Linux App Summit attendance is free of charge, but you 
must be registered to attend the event. 

If you need a visa to enter Spain for the conference, here's how 
to request a [visa invitation letter](https://linuxappsummit.org/visa). 

 <button class="button" onclick="window.location.href='https://events.linuxappsummit.org/'">Register!</button>

You should also join our [Telegram attendees channel](https://t.me/joinchat/Dbb0PlJjx4tPFCSu2dd7PA).
