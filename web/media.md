---
layout: page
title: "Media"
permalink: /media
---

# Linux App Summit in Photos and Videos

If you weren't able to participate in person at LAS 2019, or even if you were, 
we have collected photos and videos of the event for you. 

## Talks
We were able to stream most talks this year on 
[YouTube](https://www.youtube.com/channel/UCjSsbz2TDxIxBEarbDzNQ4w) 
and [Twitter LIVE](https://twitter.com/LinuxAppSummit). 

## Event Photos

Please join us in sharing photos from the event. Check back for more updates.  

 <button class="button" onclick="window.location.href='https://las2019.bcnfs.org/doku.php?id=wiki:lasphotos'">View or Share Photos</button>