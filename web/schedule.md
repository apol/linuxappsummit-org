---
layout: page
title: "Schedule"
permalink: /schedule/
---

# Conference Schedule
Here is a snapshot of the schedule and the social events we are planning. 

A more [detailed schedule](https://conf.linuxappsummit.org/en/LAS2019/public/schedule)
is available, where you can also see a list of our wonderful 
[speakers](https://conf.linuxappsummit.org/en/LAS2019/public/speakers), or browse
[talks by topic](https://conf.linuxappsummit.org/en/LAS2019/public/events).

### Monday, Nov 11
* 18:00 to 20:00 - Pre-registration Social Event at 
[Lleialtat Santsenca](https://linuxappsummit.org/location/) (venue), 
on the ground floor.

### Tuesday, Nov 12
* **[Tuesday schedule](https://conf.linuxappsummit.org/en/LAS2019/public/schedule/1)**
* Optional Walking Tour: For those who'd like to explore Barcelona, there 
will be a professional guide that takes our group on a 1.5 hr walk. 
Cost is 5 EUR each and we will meet the guide at 19:00 in front of 
Casa Batlló (Passeig de Gràcia, 43, 08007 Barcelona). [Learn More](https://las2019.bcnfs.org/doku.php?id=wiki:bcntour). 

### Wednesday, Nov 13
* **[Wednesday schedule](https://conf.linuxappsummit.org/en/LAS2019/public/schedule/2)**
* Sponsors Dinner: This dinner is for sponsors only. More details have 
been sent to those attending.

*Group photo will be taken this day at 16:10*

### Thursday, Nov 14
* **[Thursday schedule](https://conf.linuxappsummit.org/en/LAS2019/public/schedule/3)**
* Social Event: Everyone is invited to join us for drinks and tapas as we 
close the core conference days. [Details are here](https://linuxappsummit.org/social-events/). 
Feel free to bring friends!

### Friday, Nov 15
* **[BoFs and Hacking!](https://las2019.bcnfs.org/doku.php?id=wiki:bofs)**

<div class="las-banner" style="background-color: #982c7e; color: white;" markdown="1">
# Schedule FAQs
### What is an unconference? 
Unconference time is aside to help people talk about the topics that 
will be most useful to them during the Linux App Summit. 

We do our best to select a variety of interesting talks for participants to hear 
throughout the day, but we know that sometimes people have
ideas sparked throughout the conference and want to talk about them, 
or that they may not see a talk on the schedule that they really wished was 
there. This is what unconference time is for. 

Each morning, people will be able to write ideas for unconference topics. 
In the afternoon, participants get to vote for the topics they are most 
interseted in. The two most popular topics will be chosen each day. If your
topic is not chosen, feel free to propose it again the next day.


### What is a BoF? 
BoF stands for "Birds of a Feather," and is from the idiom "Birds of a feather 
flock together." 

The idea is that people who are interested in the same topic 
will get together during a conference to discuss and/or work on that topic.

Since people from all over the world will be attending the Linux App Summit, 
it's a great place for collaboration to happen. 

### Will there be more social events planned? 
Yes, we are working on having some fun options for participants!

</div>
