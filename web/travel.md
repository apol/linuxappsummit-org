---
layout: page
title: "Travel"
permalink: /travel/
---

# Travel and Transportation

The [Barcelona Airport](https://en.wikipedia.org/wiki/Barcelona%E2%80%93El_Prat_Josep_Tarradellas_Airport) 
has plenty of connections both with Europe and with America/Asia
so getting here should not be a problem.

The city also has High Speed trains to Madrid, Paris, and other European cities.
As mentioned the venue is very close to the main central station of Sants Estació (10
minutes by foot). All trains, local, regional and international call at this station, including the
line that connects with the airport. It also has connections with 2 metro lines, and lots of
local and regional buses.

The Barcelona Metro and Bus system is quite good and one should be able to get
anywhere in the city with it. The closest metro station to the venue is Plaça de Sants
