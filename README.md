# Test environment
The easiest way I've found to test the website is by using docker:

```
docker run --rm --volume=$PWD:/srv/jekyll -p 4000:4000 -it jekyll/jekyll:3.8 bash
```

Once inside, you run:
```
jekyll serve -s web
```

Then the website should be accessible through http://0.0.0.0:4000/.
